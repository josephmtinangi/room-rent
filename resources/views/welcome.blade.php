@extends('layouts.app')

@section('content')
<div class="jumbotron">
    <div class="container text-center">
        <h1>Room Finder</h1>
        <p>Find room to rent the easy way</p>
    </div>
</div>
@endsection
